function potencia(base, exponente) {
	var producto = 1,
		contador = 1;

	while(contador <= exponente) {
		producto = producto * base;
		contador++;
	}

	return producto;
}

console.log(potencia(2, 3));


function potencia1(base, exponente) {
	var producto = 1,
		contador = 1;

	if(exponente == 0) {
		producto = 1;
		return producto;
	}
	do {
		producto = producto * base;
		contador++;	
		
	}
	while(contador <= exponente);

	return producto;
}

console.log(potencia1(2, 0));

function potencia2(base, exponente) {
	var producto = 1
	if(exponente == 0) {
		producto = 1;
		return producto;
	}
	for(var contador = 1; contador <= exponente; contador++) {
		producto = producto * base;
	}

	return producto;
}

console.log(potencia2(2, 3));
