 function Animal(edad) {
	this.edad = edad || 0;
}

Animal.prototype.crecer = function() {
	this.edad = this.edad + 1;
	return this.edad;
}

var iris = new Animal(6);
var rufo = new Animal(4);

console.log(iris instanceof Animal);
console.log(rufo.crecer());
