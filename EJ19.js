var http = require('http').createServer(webServer),
	fs = require('fs'),
	index = fs.createReadStream('index1.html');

function webServer(req, res) {
	res.writeHead(200, {'Content-Type': 'text/html'});
	index.pipe(res);
}

http.listen(3001);