'use strict'

var fs = require('fs'),
	Q = require('q'),
	file = './assets/entrada.txt',
	newFile = './assets/entrada-callback.txt';

function existFile(file) {
	let defer = Q.defer();

	fs.access(file, fs.F_OK, (err) => {
		return (err) ? defer.reject(new Error('No existe')) : defer.resolve(true);
	});

	return defer.promise
}

function readFile(file) {
	let defer = Q.defer();

	console.log('Existe');

	fs.readFile(file, (err, data) => {
		return (err) ? defer.reject(new Error('No se pudo leer')) : defer.resolve(data);
	});
	return defer.promise;
}

function writeFile(file, data) {
	let defer = Q.defer();

	console.log('Leido');

	fs.writeFile(file, data, (err) => {
		return (err) ? defer.reject(new Error('No se pudo copiar')) : defer.resolve('Se copio');
	});
	return defer.promise;
}

/*
 - Leer
 - Copiar
 - Avisar que se copio
 - Maneja errores
*/

existFile(file)
	.then(() => readFile(file))
	.then((dataPromise) => writeFile(newFile, dataPromise))
	.then((dataPromise) => { console.log(dataPromise) })
	.fail((err) => { console.log(err) });
