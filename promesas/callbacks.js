'use strict'

var fs = require('fs'),
	file = './assets/entrada.txt',
	newFile = './assets/entrada-callback.txt';

fs.access(file, fs.F_OK, (err) => {
	if(err) {
		console.log(err);
	} else {
		fs.readFile(file, (err, data) => {
			if(err) {
				console.log(err);
			} else {
				fs.writeFile(newFile, data, (err) => {
					return (err) ? console.log('No se pudo copiar') : console.log('se copio');
				});
			}
		});
	}
});