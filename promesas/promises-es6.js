'use strict'

var fs = require('fs'),
	file = './assets/entrada.txt',
	newFile = './assets/entrada-callback.txt',
	promise = new Promise((resolve, reject) => {
		fs.access(file, fs.F_OK, (err) => {
			return (err) ? reject(new Error('No existe')) : resolve(true);
		})
	});

promise.then((dataPromise) => {
	console.log('Existe');
	return new Promise((resolve, reject) => {
		fs.readFile(file, (err, data) => {
			return (err) ? reject(new Error('No se pudo leer')) : resolve(true);
		})
	})
})
.then((dataPromise) => {
	console.log('Leido');
	return new Promise((resolve, reject) => {
		fs.writeFile(newFile, dataPromise, (err) => {
			return (err) ? reject(new Error('No se copio')) : resolve('Se copio');
		})
	})
})
.then((dataPromise) => { console.log(dataPromise) })
.catch((err) => { console.log(err) });
