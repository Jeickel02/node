 function Animal(edad) {
	this.edad = edad || 0;
}

Animal.prototype.crecer = function() {
	this.edad = this.edad + 1;
	return this.edad;
}

function Felino() {}

Felino.prototype = new Animal();
Felino.prototype.constructor = Felino;

Felino.prototype.maullar = function() {
	console.log('Miau..!!!');
}

var iris = new Felino(2);

console.log(iris.crecer());
console.log(iris.maullar());