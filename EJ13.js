var http = require('http');
var fh = require('./modFechaHora');

http.createServer(function(req, res) {
	res.writeHead(200, {'Content-Type': 'text/html'});
	res.write('La fecha y la hora es: ' + fh.miFechaHora());
	res.end();
}).listen(7001);