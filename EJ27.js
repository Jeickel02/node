var http = require('http').createServer(webServer),
	form = require('fs').readFileSync('form1.html'),
	querystring = require('querystring'),
	util = require('util'),
	url = require('url'),
	dataString = '';

function webServer(req,res){
	if(req.method=='GET'){
		res.writeHead(200,{'Content-type':'text/html'});
		res.end(form);
	}
	if(req.method=='POST'){
		req
			.on('data',function(data){
				dataString += data;
			})
			.on('end',function(){
				var templateString = `Los datos enviados por POST como string son: ${dataString}`;
				console.log(templateString);
				res.end(templateString);
			})
	}
}
http.listen(3001);
