var http = require('http').createServer(webServer),
	fs = require('fs');

function webServer(req, res) {
	function readFile(err, data) {
		if(err) throw err
		res.end(data);
	}
	res.writeHead(200, {'Content-Type': 'text/html'});
	fs.readFile('index2.html', readFile)
}

http.listen(3001);