const EventEmitter = require('events');

class MyStream extends EventEmitter {
	constructor() {
		super();
	}

	write(data) {
		this.emit('data', data);
	}
}

const stream = new MyStream();
stream.on('data', (data) => {
	console.log('Data recibida:', data);
});

stream.write('Herencia');