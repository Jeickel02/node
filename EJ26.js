var http = require('http');
var options = {
	host: 'www.mediotiempo.com',
	port: 80,
	path: '/carton.php?id_carton=8638'
}
var htmlCode = '';

function httpClient(res) {
	console.log(`El sitio ${options.host} ha respondido con el estado ${res.statusCode}`);
	res.on('data', (data) => {
		htmlCode += data;
		console.log(data, data.toString());
	})
}

function httpError(err) {
	console.log(err);
}

function webServer(req, res) {
	res.writeHead(200, {'Content-Type': 'text/html'});
	res.end(htmlCode);
}

http.get(options, httpClient)
	.on('error', httpError);

http.createServer(webServer)
	.listen(3001);