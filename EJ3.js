var buf = Buffer.alloc(15);
buf[0] = 255;
console.log(buf);

var buf1 = new Buffer([10, 20, 30, 40, 50]);
console.log(buf1);

var buf2 = new Buffer('www.uneweb.edu.ve', 'utf-8');
console.log(buf2);