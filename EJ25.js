var http = require('http');
var options = {
	host: 'uneweb.edu.ve',
	port: 80,
	path: '/'
}

http.get(options, (res) => {
	console.log(`El sitio ${options.host} ha respondio. Codigo de estado: ${res.statusCode}`);
}).on('error', (err) => {
	console.log(err);
});
