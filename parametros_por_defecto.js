function myFunction(x, y = 10) {
	return x + y;
}

console.log(myFunction(10));


var numbers = [4, 9, 16, 25, 29];

var firstIndex = numbers.findIndex(myFunction);
var first = numbers.find(myFunction);
console.log(first);
console.log(firstIndex);

function myFunction(value, index, array) {
	return value > 18;
}

var a = Number.EPSILON,
	b = Number.MIN_SAFE_INTEGER,
	c = Number.MAX_SAFE_INTEGER;
console.log(a);
console.log(b);
console.log(c);

console.log(Number.isSafeInteger(12345678901234567890));
console.log(Number.isInteger(10));

console.log(Number.isFinite(10/0));
console.log(Number.isFinite(10/1));

console.log(isNaN('Hola'));