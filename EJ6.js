const eventEmitter = require('events');
class MyEmitter extends eventEmitter {}
const myEmitter = new MyEmitter();

myEmitter.on('evento', () => {
	console.log('Un evento ha ocurrido');
});

myEmitter.emit('evento');
