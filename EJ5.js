var fs = require('fs');
var data = '';
var readerStream = fs.createReadStream('entrada.txt');
readerStream.setEncoding('UTF8');

readerStream.on('data', function(pedazo) {
	data += pedazo;
});

readerStream.on('end', function() {
	console.log(data);
});

readerStream.on('error', function(err) {
	console.error(err.stack);
});
