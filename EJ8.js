function Animal(edad) {
	this.edad = edad || 0;
}

var iris = new Animal(6);
var rufo = new Animal(4);

console.log(iris instanceof Animal);
console.log(rufo.edad);